var game = null;

window.onload = function() {
	var config = {
		type: Phaser.AUTO,
		// width: 800,
		// height: 600,
		pixelArt: true,
		scene: {
			preload: preload,
			create: create,
			update: update
		},
		scale: {
			mode: Phaser.Scale.WIDTH_CONTROLS_HEIGHT,
			width: 360,
			height: 270
		}
	};

    game = new Phaser.Game(config);

	let controls;
	let path = {
		// tiles : 'assets/tiles_dungeon_v1.1.png',
		// map: 'assets/map.tmx'
		tiles : 'https://glcdn.githack.com/tecsup-ljimenez/canvas-template-libgdx/raw/master/assets/tiles_dungeon_v1.1.png',
		map: 'https://glcdn.githack.com/tecsup-ljimenez/canvas-template-libgdx/raw/master/assets/map.tmx'
	};

	function preload() {
		console.log('preload')

		this.load.crossOrigin = 'Anonymous';
		this.load.image('tiles', path.tiles);
  		this.load.tilemapTiledJSON('map', path.map);

		this.load.spritesheet('player', 
			'https://raw.githubusercontent.com/sparklinlabs/superpowers-asset-packs/master/prehistoric-platformer/characters/playable/girl.png',
			{ frameWidth: 64, frameHeight: 58 }
		);
	}

	function create() {
		console.log('create')
		
		this.add.image(400, 300, 'player');
		const map = this.make.tilemap({ key: 'map' });

		const tileset = map.addTilesetImage('dungeon_tileset_v1', 'tiles');

		const belowLayer = map.createStaticLayer('Floor/walls', tileset, 0, 0);
		const worldLayer = map.createStaticLayer('Objects (under)', tileset, 0, 0);
		const aboveLayer = map.createStaticLayer('Objects', tileset, 0, 0);


		const camera = this.cameras.main;
		const cursors = this.input.keyboard.createCursorKeys();
		controls = new Phaser.Cameras.Controls.FixedKeyControl({
			camera: camera,
			left: cursors.left,
			right: cursors.right,
			up: cursors.up,
			down: cursors.down,
			speed: 0.5
		});

		camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
	}

	function update(time, delta) {
		// console.log('update')

		controls.update(delta);
	}
};
